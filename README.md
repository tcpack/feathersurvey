# FeatherSurvey

Next things:

* Server Side Check if Survey Already Submitted
* Check if CurrentUses = MaxUses

Project Structure
-----------------

| Name                               | Description                                                  |
| ---------------------------------- | ------------------------------------------------------------ |
| **config**/passport.js             | Passport Local and OAuth strategies, plus login middleware.  |
| **controllers**/api.js             | Legacy                                                       |
| **controllers**/contact.js         | Controller for contact form.                                 |
| **controllers**/home.js            | Controller for home page (index).                            |
| **controllers**/user.js            | Controller for user account management.                      |
| **controllers**/template.js        | Controller for user template management.                     |
| **controllers**/survey.js          | Controller for user survey management.                       |
| **controllers**/result.js          | Controller for user result management.                       |
| **models**/Config.js               | Legacy.                                                      |
| **models**/Result.js               | Mongoose schema and model for Result.                        |
| **models**/Survey.js               | Mongoose schema and model for Survey.                        |
| **models**/Template.js             | Mongoose schema and model for Template.                      |
| **models**/User.js                 | Mongoose schema and model for User.                          |
| **public**/                        | Static assets (fonts, css, js, img).                         |
| **public**/**js**/main.js          | Place your client-side JavaScript here.                      |
| **public**/**css**/main.scss       | Main stylesheet for your app.                                |
| **public/css/themes**/default.scss | Some Bootstrap overrides to make it look prettier.           |
| **views/account**/                 | Templates for *login, password reset, signup, profile*.      |
| **views/api**/                     | Templates for API Examples.(Legacy)                          |
| **views/survey**/                  | Templates for survey related sites.                          |
| **views/template**/                | Templates for template related sites.                        |
| **views/partials**/flash.pug       | Error, info and success flash notifications.                 |
| **views/partials**/header.pug      | Navbar partial template.                                     |
| **views/partials**/footer.pug      | Footer partial template.                                     |
| **views**/surveylayout.pug         | Custom base template to load custom .js and .css             |
| **views**/layout.pug               | Base template.                                               |
| **views**/home.pug                 | Home page template.                                          |
| .env.example                       | Your API keys, tokens, passwords and database URI.           |
| app.js                             | The main application file.                                   |
| package.json                       | NPM dependencies.                                            |
| yarn.lock                          | Contains exact versions of NPM dependencies in package.json. |
