const Template = require('../models/Template');

/**
 * GET /templates
 * New Survey Form
 */
exports.getTemplates = (req, res) => {
  if (req.user.userRole != 'Admin') {
    req.flash('error', { msg: 'No permission!' });
    return res.redirect('/');
  } else {
    Template.getAllTemplates((err, data) => {
      if (err) { return next(err); }
      res.render('template/templates', {
        title: 'All the little templates', templates: data
      });
    });
  }
};



/**
 * GET /templates/new
 * New Survey Form
 */
exports.getTemplateEditor = (req, res) => {
  if (req.user.userRole != 'Admin') {
    req.flash('error', { msg: 'No permission!' });
    return res.redirect('/templates');
  } else {
    res.render('template/template-add', {
      title: 'A new template'
    });
  }
};

/**
 * POST /templates/new
 * Add a new template
 */
exports.addTemplate = (req, res, next) => {
  if (req.user.userRole != 'Admin') {
    req.flash('error', { msg: 'No permission!' });
    return res.redirect('/templates');
  } else {

    req.assert('name', 'Name must be at least 4 characters long').len(4);
    req.assert('surveyjson', 'JSON must be at least 10 characters long').len(10);

    const errors = req.validationErrors();

    if (errors) {
      req.flash('errors', {msg:errors});
      return res.redirect('/templates');
    }

    const template = new Template({
      name: req.body.name,
      description: req.body.description || '',
      category: req.body.description || '',
      json: 'var surveyJSON = ' + req.body.surveyjson
    });

    Template.findOne({ name: req.body.name }, (err, existingTemplate) => {
      if (err) { return next(err); }
      if (existingTemplate) {
        req.flash('errors', { msg: 'Template with that name already exists.' });
        return res.redirect('/templates');
      }
      template.save((err) => {
        if (err) { return next(err); }
        req.flash('success', { msg: 'Template has been saved' });
        res.redirect('/templates');
      });
    });
  }
};

/**
 * GET /templates/:id
 * Edit template
 */
exports.editTemplate = (req, res) => {
  if (req.user.userRole != 'Admin') {
    req.flash('error', { msg: 'No permission!' });
    return res.redirect('/templates');
  } else {
    Template.findById(req.params.id , (err, existingTemplate) => {
      res.render('template/template-editor', {
        title: 'Edit - ' + req.params.name, template: existingTemplate
      });
    });
  }
};

/**
 * POST /templates/:id
 * Save template from Edit
 */
exports.saveTemplate = (req, res) => {
  if (req.user.userRole != 'Admin') {
    req.flash('error', { msg: 'No permission!' });
    return res.redirect('/templates');
  } else {

    req.assert('name', 'Name must be at least 4 characters long').len(4);
    req.assert('surveyjson', 'JSON must be at least 10 characters long').len(10);

    const errors = req.validationErrors();

    if (errors) {
      req.flash('errors', errors);
      return res.redirect('/templates');
    }

    const template = {
      name: req.body.name,
      description: req.body.description || '',
      category: req.body.category || '',
      json: 'var surveyJSON = ' + req.body.surveyjson
    };
     console.log(req.params.id);
    Template.findByIdAndUpdate(req.params.id ,{$set:template}, (err,data) => {
      if (err) {req.flash('errors', err);return res.redirect('/templates');}
      req.flash('success', { msg: 'Template has been updated' });
      res.redirect('/templates');
    });
  }
};

/**
 * GET /templates/:name/delete
 * Delete user account.
 */
exports.deleteTemplate = (req, res, next) => {
  Template.remove({_id:req.params.id}, (err,data) => {
    if (err) { return next(err); }
    req.flash('info', { msg: 'Template has been deleted' });
    res.redirect('/templates');
  });
};