const bcrypt = require('bcrypt');
const passport = require('passport');

const Result = require('../models/Result');
const Survey = require('../models/Survey');
const User = require('../models/User');

/**
 * GET /result/:id
 * Survey Home page.
 */
exports.getSurveyResults = (req, res) => {
  Survey.isAuthorized(req.params.id, req.user._id, (err) => {
    if (err) { return next(err); }
    Result.getAllResultsById(req.params.id, (err, data) => {
      if (err) { return next(err); }
      console.log(data);
    });
  })
};


/**
 * POST /result/:id
 * Post
 */
exports.postResult = (req, res) => {
  var sessionHash = req.session.surveySession || "";
  Survey
    .findById(req.params.id)
    .exec((err, survey) => {
      if (err) { return next(err); }
      if (!survey) {
        req.flash('errors', { msg: 'Survey is not available' });
        return res.redirect('/');
      } else if (!bcrypt.compareSync(survey.password, sessionHash)) {
        req.flash('errors', { msg: 'Wrong authentication please try again' });
        res.render('survey/survey-home', {
          title: 'Login to Survey: ' + survey.name, surveyname: req.params.name
        });
      } else {
        Result
          .findOne({ cookie: req.cookies['connect.sid'] })
          .exec((err, result) => {
            if (err) { return next(err); }

            if (result) {
              console.log(result);
              req.flash('errors', { msg: 'Already submitted this survey' });
              return res.send({ error: 1 });
            } else {
              const result = new Result({
                surveyId: req.params.id,
                cookie: req.cookies['connect.sid'],
                json: req.body
              });
              result.save((err) => {
                if (err) { return next(err); }
                survey.currentUses = survey.currentUses + 1;
                survey.save((err) => {
                  if (err) { return next(err); }
                  req.flash('success', { msg: 'Result submitted' });
                  res.redirect('/');
                });
              });
            }
          });
      }
    });
};