const bcrypt = require('bcrypt');
const passport = require('passport');

const Survey = require('../models/Survey');
const Template = require('../models/Template');
const User = require('../models/User');
/**
 * GET /survey
 * Survey Home page.
 */
exports.getSurveyHome = (req, res) => {
  if (req.user.userRole == 'Admin') {
      Survey.getAllSurveys((err, data) => {
      if (err) { return next(err); }
      res.render('survey/survey-home-admin', {
        title: 'All the little survey', surveys: data
      });
    });
  } else {
    Survey.getAllSurveysForUser(req.user._id,(err, data) => {
      if (err) { return next(err); }
      res.render('survey/survey-home-user', {
        title: 'All the little survey', surveys: data
      });
    });
  }
};

/**
 * GET /survey/:name
 * Survey page.
 */
exports.getSurvey = (req, res) => {
  var sessionHash = req.session.surveySession || "";
  Survey
    .findOne({ name: req.params.name })
    .exec((err, survey) => {
      if (err) { return next(err); }
      if (!survey) {
        req.flash('errors', { msg: 'Survey is not available' });
        return res.redirect('/');
      } else if (!bcrypt.compareSync(survey.password, sessionHash)) {
        res.render('survey/login', {
          title: 'Login to Survey: ' + survey.name, surveyname: req.params.name
        });
      } else {
        Template.findById(survey.templateId, (err, existingTemplate) => {
          if (err) { return next(err); }
          if (!existingTemplate) {
            req.flash('errors', { msg: 'Template is not existing' });
            return res.redirect('/survey');
          }
          res.render('survey/survey', {
            title: survey.name, survey: existingTemplate.json,surveyId:survey._id
          });
        });
      }
    });

};

/**
 * POST /survey/:name/login
 * Login to survey
 */
exports.postSurveyLogin = (req, res) => {
  req.assert('password', 'Password cannot be blank').notEmpty();
  req.assert('password', 'Password must be at least 4 characters long').len(4);
  const errors = req.validationErrors();

  if (errors) {
    req.flash('errors', errors);
    return res.render('survey/login', {
      title: 'Login to Survey ' + req.params.name, surveyname: req.params.name
    });
  }
  Survey
    .findOne({ name: req.params.name })
    .exec((err, survey) => {
      if (err) { return next(err); }
      if (!survey) {
        req.flash('errors', { msg: 'Survey is not available' });
        return res.redirect('/');
      } else {

        survey.comparePassword(req.sanitize('password').toString(), (err, isMatch) => {
          var hash = bcrypt.hashSync(survey.password, survey.passwordSalt);
          req.session.surveySession = hash;
          if (err) { console.log(err); }
          if (isMatch) {
            req.flash('success', { msg: 'Success! You are logged in.' });
            return res.redirect('/surveys/' + req.params.name);
          } else {
            req.flash('errors', { msg: 'Wrong Password' })
            return res.render('survey/login', {
              title: survey.name, name: req.params.name
            });
          }
        });
      }

    });
};

/**
 * GET /survey/new:id
 * Get survey creation form.
 */
exports.getSurveyForm = (req, res) => {
  Template.getAllTemplates((err, templates) => {
    if (err) { return next(err); }
    User.getAllUsers((err, users) => {
      if (err) { return next(err); }
      console.log(req.params.id);
      res.render('survey/survey-form', {
        title: 'Create Survey', templates: templates, users: users,selectedTemplate:req.params.id
      });
    });
  });

};

/**
 * POST /survey/new
 * Add a new survey.
 */
exports.addSurvey = (req, res, next) => {
  if (req.user.userRole != 'Admin') {
    req.flash('error', { msg: 'No permission!' });
    return res.redirect('/survey');
  } else {
    req.assert('name', 'Name must be at least 4 characters long').len(4);
    //req.assert('owners', 'Owners must be an array of owner ids').isArray();
    req.assert('maxuses', 'Max uses must be a number').isInt();
    req.assert('template', 'Template must be a id of a template').len(4);
    req.assert('password', 'Password must be at least 4 characters long').len(4);

    const errors = req.validationErrors();

    if (errors) {
      req.flash('errors', errors);
      return res.redirect('/surveys');
    }

    const survey = new Survey({
      name: req.body.name,
      owners: req.body.owners,
      maxUses: req.body.maxuses,
      currentUses: 0,
      templateId: req.body.template,
      password: req.body.password,
      passwordSalt: bcrypt.genSaltSync(10)
    });

    Survey.findOne({ name: req.body.name }, (err, existingSurvey) => {
      if (err) { return next(err); }
      if (existingSurvey) {
        req.flash('errors', { msg: 'Survey with that name already exists.' });
        return res.redirect('/surveys');
      }
      survey.save((err) => {
        if (err) { return next(err); }
        res.redirect('/surveys');
      });
    });
  }
};

/**
 * GET /survey/:name/delete
 * Delete the survey
 */
exports.deleteSurvey = (req, res, next) => {
  Survey.remove({name:req.params.name}, (err,data) => {
    if (err) { return next(err); }
    req.flash('info', { msg: 'Survey has been deleted' });
    res.redirect('/surveys');
  });
};