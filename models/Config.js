const bcrypt = require('bcrypt-nodejs');
const crypto = require('crypto');
const mongoose = require('mongoose');

const configSchema = new mongoose.Schema({
    id: String,
    resourceID: Number
}, { timestamps: true });



const Config = mongoose.model('Config', configSchema);

module.exports = Config;
