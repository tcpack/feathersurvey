const mongoose = require('mongoose');

const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;
const resultSchema = new mongoose.Schema({
  surveyId: ObjectId,
  cookie: String,
  json: Object
}, { timestamps: true });

/**
 * Helper method to get all results by their survey id
 */
resultSchema.statics.getAllResultsById = function getAllResultsById(surveyId,cb) {
    this.find({surveyId:surveyId}, (err, results) => {
    if (err) {  cb(err); }
    if (results) { cb(null,results); }
  });
};

const Result = mongoose.model('Result', resultSchema);

module.exports = Result;
