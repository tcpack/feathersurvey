const mongoose = require('mongoose');

const templateSchema = new mongoose.Schema({
  name: String,
  description: String,
  category: String,
  json: Object
}, { timestamps: true });

/**
 * Helper method to get all templates.
 */
templateSchema.statics.getAllTemplates = function getAllTemplates(cb) {
    this.find({}, (err, templates) => {
    if (err) {  cb(err); }
    if (templates) { cb(null,templates); }
  });
};

const Template = mongoose.model('Template', templateSchema);

module.exports = Template;
