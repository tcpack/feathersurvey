const bcrypt = require('bcrypt-nodejs');
const mongoose = require('mongoose');

const Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;
const surveySchema = new mongoose.Schema({
  name: String,
  password: String,
  passwordResetToken: String,
  passwordResetExpires: Date,
  passwordSalt: String,

  owners: Array,
  maxUses: Number,
  currentUses: Number,
  templateId: ObjectId
}, { timestamps: true });

/**
 * Password hash middleware.
 */
surveySchema.pre('save', function save(next) {
  const survey = this;
  bcrypt.genSalt(10, (err, salt) => {
    if (err) { return next(err); }
    bcrypt.hash(survey.password, salt, null, (err, hash) => {
      if (err) { return next(err); }
      survey.password = hash;
      next();
    });
  });
});

/** 
 * Helper method to get all owners
 */
surveySchema.statics.getAllSurveysForUser = function getAllSurveysForUser(id, cb) {
  this.find({ owners: id.toString() }, (err, surveys) => {
    if (err) { cb(err); }
    if (surveys) { cb(null, surveys); }
  });
};

/**
 * Helper method to get all surveys.
 */
surveySchema.statics.getAllSurveys = function getAllSurveys(cb) {
  this.find({}, (err, surveys) => {
    if (err) { cb(err); }
    if (surveys) { cb(null, surveys); }
  });
};

/** 
 * Helper method to check if a uId is a owner of a survey with id
 */
surveySchema.statics.isAuthorized = function isAuthorized(surveyId, uId, cb) {
  this.findById(surveyId, (err, survey) => {
    if (err) { cb(err); }
    if (!survey) { cb({ msg: 'No survey found' }); } else {
      cb(null, survey.owners.includes(uId));
    }
  });
};

/**
 * Helper method for validating Survey's password.
 */
surveySchema.methods.comparePassword = function comparePassword(candidatePassword, cb) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    cb(err, isMatch);
  });
};



const Survey = mongoose.model('Survey', surveySchema);

module.exports = Survey;
