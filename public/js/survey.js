var tmpJson = surveyJSON;
tmpJson.cookieName = $('#surveyId')[0].innerHTML;
Survey.defaultBootstrapMaterialCss.root = "row";
Survey.defaultBootstrapMaterialCss.navigationButton = "btn waves-effect waves-light";
Survey.Survey.cssType = "bootstrapmaterial";
var survey = new Survey.Model(tmpJson);
$("#surveyContainer").Survey({
    model: survey
});

var csrf_token = $('#_csrf').val();
$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader('X-CSRF-TOKEN', csrf_token);
    }
});
survey.onComplete.add(function(result) {
    var id = $('#surveyId')[0].innerHTML;
    $.ajax('/result/' + id, {
        data: JSON.stringify(result.data),
        contentType: "application/json",
        type: 'POST'
    });
});
