$(document).ready(function() {
$('.alert').append('<button class="waves-effect btn-flat myclose right"><i class="material-icons">close</i></button>');

$('body').on('click', '.alert .myclose', function() {
    $(this).parent().fadeOut(300, function() {
        $(this).remove();
    });
});

$('select').material_select();
$('.chips').material_chip();
});

